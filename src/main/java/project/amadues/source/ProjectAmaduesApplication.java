package project.amadues.source;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectAmaduesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectAmaduesApplication.class, args);
	}

}
