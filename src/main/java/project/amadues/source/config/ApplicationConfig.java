package project.amadues.source.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Value("${spring.application.product_mode}")
    private String product_mode;

    @Bean
    public String return_product_mode(){
        return product_mode;
    }
}
