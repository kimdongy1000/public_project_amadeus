package project.amadues.source.config;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class JwtKeyGenerator {

    @Value("${spring.security.keySize}")
    private int keySeize;

    @Value("${spring.security.secretKey}")
    private String secretKey;

    @Bean
    public JWK javaWebKey() throws Exception{

        JWK jwk  = new RSAKeyGenerator(keySeize).keyID(secretKey).algorithm(JWSAlgorithm.RS256).generate();
        return jwk;
    }

}
