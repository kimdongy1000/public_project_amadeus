package project.amadues.source.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;

@Configuration
public class Oauth2Config {

    @Value("${spring.security.google_client_id}")
    private String google_client_id;

    @Value("${spring.security.google_client_secret}")
    private String google_client_secret;

    @Value("${spring.security.google_redirect_url}")
    private String google_redirect_url;

    @Value("${spring.security.naver_client_id}")
    private String naver_client_id;

    @Value("${spring.security.naver_client_secret}")
    private String naver_client_secret;

    @Value("${spring.security.naver_redirect_url}")
    private String naver_redirect_url;

    @Value("${spring.security.naver_authorization_url}")
    private String naver_authorization_url;

    @Value("${spring.security.naver_token_url}")
    private String naver_token_url;

    @Value("${spring.security.naver_userInfo_url}")
    private String naver_userInfo_url;








    private ClientRegistration googleClientRegistration(){
        return CommonOAuth2Provider.GOOGLE.getBuilder("google")
                .clientId(google_client_id)
                .clientSecret(google_client_secret)
                .redirectUri(google_redirect_url)
                .build();
    }

    private ClientRegistration naverClientRegistration(){

        return ClientRegistration.withRegistrationId("naver")
                .clientId(naver_client_id)
                .clientSecret(naver_client_secret)
                .redirectUri(naver_redirect_url)
                .authorizationUri(naver_authorization_url)
                .tokenUri(naver_token_url)
                .userInfoUri(naver_userInfo_url)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .build();

    }

    @Bean
    public ClientRegistrationRepository clientRegistrationRepository(){
        ClientRegistrationRepository clientRegistrationRepository = new InMemoryClientRegistrationRepository(googleClientRegistration() , naverClientRegistration());
        return clientRegistrationRepository;

    }

}
