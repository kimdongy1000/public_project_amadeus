package project.amadues.source.config;

import com.google.gson.Gson;
import com.nimbusds.jose.jwk.JWK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;
import project.amadues.source.filter.CustomJwtAuthenticationFilter;
import project.amadues.source.repository.RedisRepository;
import project.amadues.source.security.CustomJWTParser;
import project.amadues.source.security.JWTGenerator;



/*
* 스프링 시큐리티와 관련한 모든 설정
*
* 04/20
*   /user/register api 는 permit 해제
*
*   그 외 요청은 전부 닫고
*   csrf 사용하지 않음
*   form 로그인 사용하지 않음
*   basic 로그인 사용하지 않음
*
* */


@Configuration
public class SecurityConfig {



    @Autowired
    private CustomJWTParser jwtParser;

    @Autowired
    private JWK jwk;

    @Autowired
    private JWTGenerator jwtGenerator;

    @Autowired
    private Gson gson;

    @Autowired
    private RedisRepository redisRepository;

    @Autowired
    private String return_product_mode;





    @Value("${spring.security.access_tokenExpireTime}")
    private Long access_tokenExpireTime;







    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception{

        httpSecurity.csrf().disable();
        httpSecurity.formLogin().disable();
        httpSecurity.httpBasic().disable();

        httpSecurity.authorizeRequests().antMatchers("/user/register" ,
                                                                "/user/login" ,
                                                                "/oauth2/google/oauth2_login_address" ,
                                                                "/oauth2_google_login" ,
                                                                "/oauth2/naver/oauth2_login_address" ,
                                                                "/oauth2_naver_login" ,


                                                                "/favicon.ico"


                                                                ).permitAll()
                .and().authorizeRequests().anyRequest().authenticated()
                .and().addFilterAfter(new CustomJwtAuthenticationFilter(jwtParser , jwk , jwtGenerator , gson  , redisRepository , access_tokenExpireTime , return_product_mode) , CorsFilter.class);







        return httpSecurity.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){

        return new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2A, 15 );
    }
}
