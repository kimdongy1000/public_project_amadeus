package project.amadues.source.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;


/*
 * Web MVC 관련한 모든 설정
 *
 * 1) 4/20 CORS 설정진행
 *
 * */

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${application.front.server}")
    private String front_server;



    /*
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins(front_server)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .allowedHeaders("Authorization", "Content-Type", "Accept")
                //.allowedHeaders("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Authorization, x-xsrf-token")
                .allowCredentials(true)
                .maxAge(3600); // Preflight 요청의 캐싱 시간 설정
    }

     */







}
