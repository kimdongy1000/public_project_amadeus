package project.amadues.source.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.web.bind.annotation.*;
import project.amadues.source.dto.LoginDto;
import project.amadues.source.service.LoginService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private Gson gson;
    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;





    @PostMapping("/user/login")
    public ResponseEntity<?> loginUser(
            @Valid @RequestBody LoginDto loginDto)
    {
        try{

            String to_json_result = gson.toJson(loginService.loginUser(loginDto));

            return new ResponseEntity<>(to_json_result , HttpStatus.OK);
        }catch (Exception e){
            throw new RuntimeException(e);
        }

    }

    @GetMapping("/oauth2/{oauth2_service}/oauth2_login_address")
    public ResponseEntity<?> google_oauth2_address(
            @PathVariable("oauth2_service") String oauth2_service
        )
    {

        try{

            ClientRegistration clientRegistration = null;
            StringBuffer return_url = new StringBuffer();

            switch (oauth2_service) {
                case "google" :

                    clientRegistration = clientRegistrationRepository.findByRegistrationId("google");
                    Set<String> scopes = clientRegistration.getScopes();
                    String string_scope = String.join(" ", clientRegistration.getScopes());
                    return_url.append(clientRegistration.getProviderDetails().getAuthorizationUri());
                    return_url.append("?");
                    return_url.append("client_id=");
                    return_url.append(clientRegistration.getClientId());
                    return_url.append("&");
                    return_url.append("redirect_uri=");
                    return_url.append(clientRegistration.getRedirectUri());
                    return_url.append("&");
                    return_url.append("response_type=code&");
                    return_url.append("scope=");
                    return_url.append(string_scope);

                    break;

                case "naver" :
                    clientRegistration = clientRegistrationRepository.findByRegistrationId("naver");
                    return_url.append(clientRegistration.getProviderDetails().getAuthorizationUri());
                    return_url.append("?");
                    return_url.append("response_type=code");
                    return_url.append("&");
                    return_url.append("client_id=");
                    return_url.append(clientRegistration.getClientId());
                    return_url.append("&");
                    return_url.append("state=");
                    return_url.append("STATE_URL");
                    return_url.append("&");
                    return_url.append("redirect_uri=");
                    return_url.append(clientRegistration.getRedirectUri());


                    break;

            }


            Map<String , Object> result = new HashMap<>();
           result.put("Oauth2_login_url" , return_url);


            return new ResponseEntity<>(gson.toJson(result) , HttpStatus.OK);
        }catch(Exception e){
            throw new RuntimeException(e);
        }

    }

    @GetMapping("/oauth2_{oauth2_service}_login")
    public ResponseEntity<?> google_login(
            @RequestParam("code") String code ,
            @PathVariable("oauth2_service") String oauth2_service,
            HttpServletResponse response

        )
    {
        try{

           Map<String , Object> token = loginService.oauth2_loadByUser(code , oauth2_service);

            String to_json_token_result = gson.toJson(token);
            to_json_token_result = Base64.getUrlEncoder().encodeToString(to_json_token_result.getBytes());


            Cookie cookie = new Cookie("Oauth2_JWT_Token" , to_json_token_result);
            cookie.setMaxAge(60);
            cookie.setPath("/");
            response.addCookie(cookie);

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.SET_COOKIE , cookie.toString());


            return new ResponseEntity<>(null ,headers ,HttpStatus.OK);
        }catch(Exception e){
            throw new RuntimeException(e);
        }

    }
}
