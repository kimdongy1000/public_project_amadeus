package project.amadues.source.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class MainController {

    @Autowired
    private Gson gson;


    @GetMapping("/main")
    public ResponseEntity apiTestMain(){

        try{

            Map<String , Object> result = new HashMap<>();
            result.put("test_api" , true);

            String json_result = gson.toJson(result);

            return new ResponseEntity<>(json_result , HttpStatus.OK);
        }catch(Exception e){
            throw new RuntimeException(e);
        }



    }
}
