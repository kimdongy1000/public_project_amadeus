package project.amadues.source.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import project.amadues.source.dto.UserDto;
import project.amadues.source.service.UserService;

import javax.validation.Valid;

/*
* user 등록과 관련한 controller
*
*
* */

@RestController
public class RegisterController {

    @Autowired
    private UserService userService;

    @PostMapping("/user/register")
    public ResponseEntity<?> user_register(@Valid @RequestBody UserDto userDto , BindingResult bindingResult){

        try{

            int result = userService.register_user(userDto);


            return new ResponseEntity<>(true , null , HttpStatus.OK);
        }catch(RuntimeException e){
            throw new RuntimeException(e);
        }

    }
}
