package project.amadues.source.controller.advice;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@Aspect
@Component
@Slf4j
public class BindResultAdvice {



    @Before("execution(* project.amadues.source.controller..*.*(..)) && args(.., bindingResult)")
    public void transactionStart(BindingResult bindingResult) throws Exception{

        log.info("====== BindResult 검사 시작");

        if(bindingResult.hasErrors()){

            bindingResult.getAllErrors().forEach(objectError -> {
                FieldError field = (FieldError) objectError;
                String message = objectError.getDefaultMessage();

                throw new RuntimeException(message);
            });

        }
    }


}
