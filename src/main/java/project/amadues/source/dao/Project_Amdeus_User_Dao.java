package project.amadues.source.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Project_Amdeus_User_Dao {


    private String user_email;


    private String user_password;


    private String user_auth_email;

    private String user_use_yn;

    private String user_insert_dts;

    private String user_update_dts;




}
