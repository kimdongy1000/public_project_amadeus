package project.amadues.source.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoogleOauthUserInfoDto {

    private String id;
    private String email;
    private boolean verified_email;

    private String name;

    private String given_name;

    private String picture;

    private String locale;
}
