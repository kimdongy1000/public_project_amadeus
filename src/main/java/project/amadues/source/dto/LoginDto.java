package project.amadues.source.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginDto {

    @NotBlank(message = "email 은 필수값입니다.")
    private String email;

    @NotBlank(message = "password 은 필수값입니다.")
    private String password;
}
