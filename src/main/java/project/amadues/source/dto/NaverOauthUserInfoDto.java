package project.amadues.source.dto;

import com.google.gson.internal.LinkedTreeMap;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NaverOauthUserInfoDto {

    private String id;

    private String nickname;

    private String email;

    private String name;




}
