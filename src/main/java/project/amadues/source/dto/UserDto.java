package project.amadues.source.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {

    @NotBlank(message = "firstName 은 필수값입니다.")
    private String firstName;

    @NotBlank(message = "lastName 은 필수값입니다.")
    private String lastName;
    @NotBlank(message = "email 은 필수값입니다." )
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$", message = "이메일 패턴을 확인해주세요.")
    private String email;

    @NotBlank(message = "password 은 필수값입니다.")
    private String password;
    @NotBlank(message = "confirmPassword 은 필수값입니다.")
    private String confirmPassword;

    @NotNull(message = "confirmCheckBok 은 필수값입니다.")
    private boolean confirmCheckBok;

}
