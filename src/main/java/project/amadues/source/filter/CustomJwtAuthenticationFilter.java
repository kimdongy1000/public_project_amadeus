package project.amadues.source.filter;


import com.google.gson.Gson;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.OncePerRequestFilter;
import project.amadues.source.dto.AuthenticationDto;
import project.amadues.source.repository.RedisRepository;
import project.amadues.source.security.CustomJWTParser;
import project.amadues.source.security.JWTGenerator;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.SignatureException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
public class CustomJwtAuthenticationFilter extends OncePerRequestFilter {

    private CustomJWTParser jwtParser;

    private JWK jwk;

    private JWTGenerator jwtGenerator;

    private Gson gson;

    private Long access_tokenExpireTime;

    private RedisRepository redisRepository;

    private String return_product_mode;



    public CustomJwtAuthenticationFilter(CustomJWTParser jwtParser  , JWK jwk , JWTGenerator jwtGenerator , Gson gson  , RedisRepository redisRepository , Long access_tokenExpireTime , String return_product_mode) {
        this.jwtParser = jwtParser;
        this.jwk = jwk;
        this.jwtGenerator = jwtGenerator;
        this.gson = gson;
        this.redisRepository = redisRepository;
        this.access_tokenExpireTime = access_tokenExpireTime;
        this.return_product_mode = return_product_mode;
    }








    private static final String[] ALLOW_URL = {
                "/user/register"
                , "/user/login"
                , "/oauth2/google/oauth2_login_address"
                , "/oauth2_google_login"
                , "/oauth2/naver/oauth2_login_address"
                , "/oauth2_naver_login"


                ,"/favicon.ico"
    };

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String authorizationHeader = request.getHeader("Authorization");
        String authorizationHeader_refresh = request.getHeader("Authorization_Refresh");
        final String url = request.getRequestURI();
        String method = request.getMethod();

        List<String> allow_url_matcher = Arrays.stream(ALLOW_URL).filter(x -> url.equals(x)).collect(Collectors.toList());


        if (allow_url_matcher.size() > 0 || "OPTIONS".equals(method.toUpperCase())) {
            filterChain.doFilter(request, response);
        } else {

            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ") && authorizationHeader_refresh != null && authorizationHeader_refresh.startsWith("Bearer ")) {

                String accessToken = authorizationHeader.substring(7); // "Bearer " 다음의 값만 추출 access_token
                String refreshToken = authorizationHeader_refresh.substring(7); // "Bearer " 다음의 값만 추출 refresh_token


                try {

                    RSASSAVerifier rsassaVerifier = new RSASSAVerifier((RSAKey) jwk.toPublicJWK());
                    SignedJWT access_token_signedJWT = SignedJWT.parse(accessToken);
                    boolean access_verify = access_token_signedJWT.verify(rsassaVerifier);

                    Authentication authentication = null;


                    if(!access_verify){
                        throw new SignatureException("잘못된 서명입니다.");
                    }

                    JWTClaimsSet access_token_jwtClaimsSet = access_token_signedJWT.getJWTClaimsSet();

                    Date jwt_access_token_expire_time =  access_token_jwtClaimsSet.getExpirationTime();
                    Date today = new Date();
                    Date today_add_expire_time = new Date(today.getTime());

                    if(jwt_access_token_expire_time.getTime() <= today_add_expire_time.getTime()){

                        SignedJWT refresh_token_signedJWT = SignedJWT.parse(refreshToken);
                        boolean refresh_token_verify = refresh_token_signedJWT.verify(rsassaVerifier);

                        if(!refresh_token_verify){
                            throw new SignatureException("잘못된 서명입니다.");
                        }

                        JWTClaimsSet refresh_token_jwtClaimsSet = refresh_token_signedJWT.getJWTClaimsSet();
                        Date jwt_refresh_token_expire_time =  refresh_token_jwtClaimsSet.getExpirationTime();

                        if(jwt_refresh_token_expire_time.getTime() <= today_add_expire_time.getTime()){
                            throw new RuntimeException("시간이 만료된 토큰입니다.");
                        }

                        String saved_refresh_token_authentication = redisRepository.select_redis_data(refreshToken).toString();

                        if("Y".equals(return_product_mode)){
                            boolean is_del_token =  redisRepository.delete_redis_data(refreshToken);
                            log.info("{}" , is_del_token);

                        }else{
                            boolean is_expired_token = redisRepository.expire_redis_data(refreshToken , 30  , TimeUnit.SECONDS);
                            log.info("{}" , is_expired_token);

                        }


                        AuthenticationDto refresh_authentication = gson.fromJson(saved_refresh_token_authentication , AuthenticationDto.class);

                        List<GrantedAuthority> array_authority = refresh_authentication.getAuthorities().stream().map(x -> new SimpleGrantedAuthority(x.get("role").toString())).collect(Collectors.toList());
                        authentication = new UsernamePasswordAuthenticationToken(refresh_authentication.getPrincipal() , refresh_authentication.getCredentials() , array_authority);

                        Map<String , Object> result_token = jwtGenerator.jwtGenerator(authentication , jwk);

                        response.setHeader("Authorization" , "Bearer " + result_token.get("ACCESS_TOKEN"));
                        response.setHeader("Authorization_Refresh" , "Bearer " + result_token.get("REFRESH_TOKEN"));





                        SecurityContextHolder.getContext().setAuthentication(authentication);
                        filterChain.doFilter(request, response);

                        return;
                    }

                    String username = (String)access_token_jwtClaimsSet.getClaim("username");
                    List<String> authority = (List<String>) access_token_jwtClaimsSet.getClaim("authority");


                    List<GrantedAuthority> array_authority = authority.stream().map(x -> new SimpleGrantedAuthority(x)).collect(Collectors.toList());

                    UserDetails userDetails = new User(username , UUID.randomUUID().toString() , array_authority);
                    authentication = new UsernamePasswordAuthenticationToken(userDetails , null , array_authority);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    filterChain.doFilter(request, response);



                } catch (Exception e) {
                    logger.error("===================================");

                    logger.error(e);

                    logger.error("===================================");
                    response.setStatus(HttpStatus.UNAUTHORIZED.value());
                    response.getWriter().write("UnAuthorized User");
                }
            } else {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.getWriter().write("UnAuthorized User");


            }
        }
    }
}
