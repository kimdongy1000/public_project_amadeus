package project.amadues.source.repository;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import project.amadues.source.dao.Project_Amdeus_User_Dao;

@Repository
public class LoginRepository {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;


    public Project_Amdeus_User_Dao loadUserByUsername(String username) {
        return this.sqlSessionTemplate.selectOne(this.getClass().getName() + ".loadUserByUsername" , username);
    }
}
