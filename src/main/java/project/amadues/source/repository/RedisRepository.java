package project.amadues.source.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.print.attribute.standard.JobKOctets;
import java.util.concurrent.TimeUnit;

@Repository
public class RedisRepository {

    @Autowired
    private RedisTemplate<String , Object> redisTemplate;



    public void insert_redis_data(String key , Object value , long timeOut , TimeUnit timeUnit){
        redisTemplate.opsForValue().set(key , value , timeOut , timeUnit);
    }

    public Object select_redis_data(String key){
        return redisTemplate.opsForValue().get(key);
    }

    public boolean delete_redis_data(String key){
        return redisTemplate.delete(key);
    }

    public boolean expire_redis_data(String key , long timeOut , TimeUnit timeUnit){
        return redisTemplate.expire(key , timeOut , timeUnit);
    }






}
