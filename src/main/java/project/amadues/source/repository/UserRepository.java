package project.amadues.source.repository;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import project.amadues.source.dao.Project_Amdeus_User_Dao;

@Repository
public class UserRepository {

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    public int register_user(Project_Amdeus_User_Dao dao) {

        return sqlSessionTemplate.insert(this.getClass().getName() + ".register_user" , dao);
    }
}
