package project.amadues.source.security;

import com.google.gson.Gson;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import project.amadues.source.dto.AuthenticationDto;
import project.amadues.source.repository.RedisRepository;

import java.security.SignatureException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CustomJWTParser {

    @Autowired
    private JWK jwk;

    @Value("${spring.security.access_tokenExpireTime}")
    private Long access_tokenExpireTime;

    @Autowired
    private RedisRepository redisRepository;

    @Autowired
    private Gson gson;

    public Authentication jwtParse(String accessToken , String refresh_token) throws ParseException, JOSEException  , SignatureException , ExpiredJwtException {

        RSASSAVerifier rsassaVerifier = new RSASSAVerifier((RSAKey) jwk.toPublicJWK());

        SignedJWT access_token_signedJWT = SignedJWT.parse(accessToken);
        boolean access_verify = access_token_signedJWT.verify(rsassaVerifier);

        if(!access_verify){
            throw new SignatureException("잘못된 서명입니다.");
        }

        JWTClaimsSet access_token_jwtClaimsSet = access_token_signedJWT.getJWTClaimsSet();

        Date jwt_access_token_expire_time =  access_token_jwtClaimsSet.getExpirationTime();
        Date today = new Date();
        Date today_add_expire_time = new Date(today.getTime());

        if(jwt_access_token_expire_time.getTime() <= today_add_expire_time.getTime()){

            SignedJWT refresh_token_signedJWT = SignedJWT.parse(refresh_token);
            boolean refresh_token_verify = refresh_token_signedJWT.verify(rsassaVerifier);



            if(!refresh_token_verify){
                 throw new SignatureException("잘못된 서명입니다");
            }

            JWTClaimsSet refresh_token_jwtClaimsSet = refresh_token_signedJWT.getJWTClaimsSet();
            Date jwt_refresh_token_expire_time =  refresh_token_jwtClaimsSet.getExpirationTime();
            if(jwt_refresh_token_expire_time.getTime() <= today_add_expire_time.getTime()){
                throw new RuntimeException("시간이 만료된 토큰입니다");
            }

            String saved_refresh_token_authentication = redisRepository.select_redis_data(refresh_token).toString();
            AuthenticationDto refresh_authentication = gson.fromJson(saved_refresh_token_authentication , AuthenticationDto.class);

            List<GrantedAuthority> array_authority = refresh_authentication.getAuthorities().stream().map(x -> new SimpleGrantedAuthority(x.get("role").toString())).collect(Collectors.toList());

            UserDetails userDetails = new User(refresh_authentication.getPrincipal() , refresh_authentication.getCredentials() , array_authority);
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails , null , array_authority);

            boolean is_del_token = redisRepository.delete_redis_data(refresh_token);
            if(is_del_token){
                log.info("{}" , this.getClass().getName() + "delete_refresh_token is complete");
            }

            return authentication;
        }





        String username = (String)access_token_jwtClaimsSet.getClaim("username");
        List<String> authority = (List<String>) access_token_jwtClaimsSet.getClaim("authority");


        List<GrantedAuthority> array_authority = authority.stream().map(x -> new SimpleGrantedAuthority(x)).collect(Collectors.toList());

        UserDetails userDetails = new User(username , UUID.randomUUID().toString() , array_authority);
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(userDetails , null , array_authority);

        return authenticationToken;


    }


}
