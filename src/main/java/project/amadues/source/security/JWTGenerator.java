package project.amadues.source.security;


import com.google.gson.Gson;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import project.amadues.source.repository.RedisRepository;

import java.security.DrbgParameters;
import java.security.PrivateKey;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class JWTGenerator {

    @Value("${application.backend.server}")
    private String backend_server;

    @Value("${spring.security.access_tokenExpireTime}")
    private Long access_tokenExpireTime;

    @Value("${spring.security.refresh_tokenExpireTime}")
    private Long refresh_tokenExpireTime;

    @Autowired
    private RedisRepository redisRepository;

    @Autowired
    private Gson gson;


    public Map<String , Object> jwtGenerator(Authentication authentication , JWK jwtKeyGenerator) throws Exception{

        /*access_token*/
        JWK jwk =  jwtKeyGenerator;

        JWSAlgorithm jwsAlgorithm = (JWSAlgorithm) jwk.getAlgorithm();
        String keyId = jwk.getKeyID();
        PrivateKey privateKey =  jwk.toRSAKey().toPrivateKey();

        JWSHeader access_jwtHeader = new JWSHeader.Builder(jwsAlgorithm).keyID(keyId).build();

        List<String> array_authority =  authentication.getAuthorities().stream().map(x ->{ return x.toString();}).collect(Collectors.toList());


        JWTClaimsSet access_jwtPayload = new JWTClaimsSet.Builder()
                .subject("access_token_user")
                .issuer(backend_server)
                .claim("username" , authentication.getPrincipal())
                .claim("authority" , array_authority)
                //.claim("authority" , "ROLE_MEMBER")
                .expirationTime(new Date(new Date().getTime() +(access_tokenExpireTime)))
                .build();

        RSASSASigner jwsSigner = new RSASSASigner(privateKey);

        SignedJWT access_signedJWT = new SignedJWT(access_jwtHeader , access_jwtPayload);

        access_signedJWT.sign(jwsSigner);

        String access_token = access_signedJWT.serialize();


        /*refresh_token*/
        JWSHeader refresh_JWTHeader = new JWSHeader.Builder(jwsAlgorithm).keyID(keyId).build();
        JWTClaimsSet refresh_jwtPayload = new JWTClaimsSet.Builder().subject("refresh_token_user")
                .issuer(backend_server)
                .claim("username" , authentication.getPrincipal().toString() + "/" + UUID.randomUUID().toString() + "/" + new Date().toString())
                .expirationTime(new Date(new Date().getTime() + (refresh_tokenExpireTime)))
                .build();

        SignedJWT refresh_signedJWT = new SignedJWT(refresh_JWTHeader , refresh_jwtPayload);
        refresh_signedJWT.sign(jwsSigner);

        String refresh_token = refresh_signedJWT.serialize();



        Map<String , Object> result_token = new HashMap<>();
        result_token.put("ACCESS_TOKEN"     , access_token);
        result_token.put("REFRESH_TOKEN"    , refresh_token);


        String refresh_redis_key = refresh_token;
        String refresh_redis_value = gson.toJson(authentication);
        redisRepository.insert_redis_data(refresh_redis_key ,refresh_redis_value , refresh_tokenExpireTime , TimeUnit.MILLISECONDS);

        return result_token;

    }
}
