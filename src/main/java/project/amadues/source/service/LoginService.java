package project.amadues.source.service;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.nimbusds.jose.jwk.JWK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import project.amadues.source.dto.*;
import project.amadues.source.repository.LoginRepository;
import project.amadues.source.security.JWTGenerator;
import project.amadues.source.service.users.CustomAuthenticationService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class LoginService {

    @Autowired
    private LoginRepository loginRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CustomAuthenticationService customAuthenticationService;

    @Autowired
    private JWTGenerator jwtGenerator;

    @Autowired
    private JWK javaWebKey;

    @Autowired
    private Gson gson;


    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;


    public Map<String , Object> loginUser(LoginDto loginDto) throws Exception{

        Authentication nonAuthentication = new UsernamePasswordAuthenticationToken(loginDto.getEmail() , loginDto.getPassword());
        Authentication authenticatedUser = customAuthenticationService.authenticate(nonAuthentication);

        return jwtGenerator.jwtGenerator(authenticatedUser , javaWebKey);
    }

    public Map<String, Object> oauth2_loadByUser(String code , String oauth_service) throws Exception{


        ClientRegistration clientRegistration = null;


        String client_id = null;
        String client_secret = null;
        String redirect_url = null;
        String token_url = null;
        String user_info = null;

        RestTemplate restTemplate = new RestTemplate();
        Map<String , Object> params = new HashMap<>();


        switch (oauth_service) {
            case "google":

                clientRegistration = clientRegistrationRepository.findByRegistrationId("google");

                client_id = clientRegistration.getClientId();
                client_secret = clientRegistration.getClientSecret();
                redirect_url = clientRegistration.getRedirectUri();
                token_url = clientRegistration.getProviderDetails().getTokenUri();
                user_info = clientRegistration.getProviderDetails().getUserInfoEndpoint().getUri();


                params.put("code" , code);
                params.put("client_id" , client_id);
                params.put("client_secret" , client_secret);
                params.put("redirect_uri" , redirect_url);
                params.put("grant_type" , "authorization_code");

                ResponseEntity<String> google_access_token_responseEntity = restTemplate.postForEntity(token_url , params , String.class);

                if(google_access_token_responseEntity.getStatusCode() != HttpStatus.OK){
                    throw new RuntimeException("구글 로그인에 실패했습니다.");
                }

                GoogleOauthTokenDto googleOauthTokenDto = gson.fromJson(google_access_token_responseEntity.getBody() , GoogleOauthTokenDto.class);

                HttpHeaders google_headers = new HttpHeaders();
                google_headers.add("Authorization" , "Bearer " + googleOauthTokenDto.getAccess_token());
                HttpEntity<MultiValueMap<String , String>> google_userInfoRequest_headers = new HttpEntity<>(google_headers);
               ResponseEntity<String> google_userInfo_responseEntity = restTemplate.exchange(user_info , HttpMethod.GET , google_userInfoRequest_headers , String.class);

                GoogleOauthUserInfoDto googleOauthUserInfoDto = gson.fromJson(google_userInfo_responseEntity.getBody() , GoogleOauthUserInfoDto.class);

                Authentication google_authentication = new UsernamePasswordAuthenticationToken( googleOauthUserInfoDto.getEmail() ,
                        UUID.randomUUID()  ,
                        Arrays.asList(new SimpleGrantedAuthority("ROLE_MEMBER")));

                return  jwtGenerator.jwtGenerator(google_authentication , javaWebKey);

            case "naver":

                clientRegistration = clientRegistrationRepository.findByRegistrationId("naver");

                client_id = clientRegistration.getClientId();
                client_secret = clientRegistration.getClientSecret();
                redirect_url = clientRegistration.getRedirectUri();
                token_url = clientRegistration.getProviderDetails().getTokenUri();
                user_info = clientRegistration.getProviderDetails().getUserInfoEndpoint().getUri().toString();

                UriComponentsBuilder naver_access_token_builder = UriComponentsBuilder.fromHttpUrl(token_url)
                        .queryParam("client_id" , client_id)
                        .queryParam("client_secret" , client_secret)
                        .queryParam("code" , code)
                        .queryParam("state" , "STATE_URL")
                        .queryParam("grant_type" , "authorization_code");

                String naver_token_url  = naver_access_token_builder.toUriString();





                ResponseEntity<String> naver_access_token_responseEntity = restTemplate.getForEntity(naver_token_url , String.class);
                if(naver_access_token_responseEntity.getStatusCode() != HttpStatus.OK){
                    throw new RuntimeException("네이버 로그인에 실패했습니다.");
                }

                NaverOauthTokenDto naverOauthTokenDto = gson.fromJson(naver_access_token_responseEntity.getBody() , NaverOauthTokenDto.class);

                HttpHeaders naver_header = new HttpHeaders();
                naver_header.add("Authorization" , "Bearer " + naverOauthTokenDto.getAccess_token());
                HttpEntity<MultiValueMap<String , String>> naver_userInfoRequest_headers = new HttpEntity<>(naver_header);
               ResponseEntity<String> naver_userInfo_responseEntity = restTemplate.exchange(user_info , HttpMethod.GET , naver_userInfoRequest_headers , String.class);

                Map<String , Object> naver_convert_result = gson.fromJson(naver_userInfo_responseEntity.getBody() , Map.class);
                LinkedTreeMap naver_response = (LinkedTreeMap) naver_convert_result.get("response");
                NaverOauthUserInfoDto naverOauthUserInfoDto = new NaverOauthUserInfoDto(naver_response.get("id").toString() , naver_response.get("nickname").toString() , naver_response.get("email").toString() ,naver_response.get("name").toString());

               Authentication naver_authentication = new UsernamePasswordAuthenticationToken(naverOauthUserInfoDto.getEmail() ,
                        UUID.randomUUID()  ,
                        Arrays.asList(new SimpleGrantedAuthority("ROLE_MEMBER")));

                return  jwtGenerator.jwtGenerator(naver_authentication , javaWebKey);

        }

        return null;


    }




}
