package project.amadues.source.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import project.amadues.source.dao.Project_Amdeus_User_Dao;
import project.amadues.source.dto.UserDto;
import project.amadues.source.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public int register_user(UserDto userDto) {

        String password = userDto.getPassword();
        String confirm_password = userDto.getConfirmPassword();

        if(!password.equals(confirm_password)){
            throw new RuntimeException("비밀번호가 서로 다릅니다.");
        }








        return userRepository.register_user(new Project_Amdeus_User_Dao().builder()
                        .user_email(userDto.getEmail())
                        .user_password(passwordEncoder.encode(userDto.getPassword()))
                .build());
    }
}
