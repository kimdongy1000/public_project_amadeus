package project.amadues.source.service.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomAuthenticationService implements AuthenticationProvider {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CustomUserDetailService customUserDetailService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        try{

            String username = authentication.getName();
            String password = authentication.getCredentials().toString();

            UserDetails userDetails = customUserDetailService.loadUserByUsername(username);

            if(!passwordEncoder.matches(password , userDetails.getPassword())){
                throw new BadCredentialsException("비밀번호가 일치하지 않습니다");
            }


            UsernamePasswordAuthenticationToken userLoginToken = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());


            return userLoginToken;


        }catch(Exception e){
            throw new RuntimeException(e.getMessage());

        }

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);

    }
}
