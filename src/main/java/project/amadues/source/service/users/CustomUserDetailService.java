package project.amadues.source.service.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import project.amadues.source.dao.Project_Amdeus_User_Dao;
import project.amadues.source.repository.LoginRepository;
import project.amadues.source.security.SecurityUsers;

import java.util.ArrayList;
import java.util.Arrays;

@Service
public class CustomUserDetailService implements UserDetailsService {


    @Autowired
    private LoginRepository loginRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Project_Amdeus_User_Dao projectAmdeusUserDao = loginRepository.loadUserByUsername(username);

        if(projectAmdeusUserDao == null){
            throw new UsernameNotFoundException("회원이 존재하지 않습니다.");
        }

        /*기본권한은 member*/



        return new SecurityUsers(
                projectAmdeusUserDao.getUser_email(),
                projectAmdeusUserDao.getUser_password(),
                Arrays.asList(new SimpleGrantedAuthority("ROLE_MEMBER")),
                true ,
                true ,
                true ,
                true
        );
    }
}
